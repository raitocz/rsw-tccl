<?php
// source: /var/www/tccl/app/config/config.neon 
// source: /var/www/tccl/app/config/config.local.neon 

class Container_fdc70f1db7 extends Nette\DI\Container
{
	protected $meta = array(
		'types' => array(
			'Nette\Object' => array(
				array(
					'application.application',
					'application.linkGenerator',
					'database.default.connection',
					'database.default.structure',
					'database.default.context',
					'http.requestFactory',
					'http.request',
					'http.response',
					'http.context',
					'nette.template',
					'security.user',
					'session.session',
					'doctrine.default.repositoryFactory',
					'doctrine.default.diagnosticsPanel',
					'doctrine.default.cacheCleaner',
					'application.1',
					'application.2',
					'application.3',
					'application.4',
					'application.5',
					'container',
				),
			),
			'Nette\Application\Application' => array(1 => array('application.application')),
			'Nette\Application\IPresenterFactory' => array(
				1 => array('application.presenterFactory'),
			),
			'Nette\Application\LinkGenerator' => array(1 => array('application.linkGenerator')),
			'Nette\Caching\Storages\IJournal' => array(1 => array('cache.journal')),
			'Nette\Caching\IStorage' => array(1 => array('cache.storage')),
			'Nette\Database\Connection' => array(
				1 => array('database.default.connection'),
			),
			'Nette\Database\IStructure' => array(
				1 => array('database.default.structure'),
			),
			'Nette\Database\Structure' => array(
				1 => array('database.default.structure'),
			),
			'Nette\Database\IConventions' => array(
				1 => array('database.default.conventions'),
			),
			'Nette\Database\Conventions\DiscoveredConventions' => array(
				1 => array('database.default.conventions'),
			),
			'Nette\Database\Context' => array(1 => array('database.default.context')),
			'Nette\Http\RequestFactory' => array(1 => array('http.requestFactory')),
			'Nette\Http\IRequest' => array(1 => array('http.request')),
			'Nette\Http\Request' => array(1 => array('http.request')),
			'Nette\Http\IResponse' => array(1 => array('http.response')),
			'Nette\Http\Response' => array(1 => array('http.response')),
			'Nette\Http\Context' => array(1 => array('http.context')),
			'Nette\Bridges\ApplicationLatte\ILatteFactory' => array(1 => array('latte.latteFactory')),
			'Nette\Application\UI\ITemplateFactory' => array(1 => array('latte.templateFactory')),
			'Latte\Object' => array(array('nette.latte')),
			'Latte\Engine' => array(array('nette.latte')),
			'Nette\Templating\Template' => array(array('nette.template')),
			'Nette\Templating\ITemplate' => array(array('nette.template')),
			'Nette\Templating\IFileTemplate' => array(array('nette.template')),
			'Nette\Templating\FileTemplate' => array(array('nette.template')),
			'Nette\Mail\IMailer' => array(1 => array('mail.mailer')),
			'Nette\Application\IRouter' => array(1 => array('routing.router')),
			'Nette\Security\IUserStorage' => array(1 => array('security.userStorage')),
			'Nette\Security\User' => array(1 => array('security.user')),
			'Nette\Http\Session' => array(1 => array('session.session')),
			'Tracy\ILogger' => array(1 => array('tracy.logger')),
			'Tracy\BlueScreen' => array(1 => array('tracy.blueScreen')),
			'Tracy\Bar' => array(1 => array('tracy.bar')),
			'WebLoader\IOutputNamingConvention' => array(
				1 => array(
					'webloader.cssNamingConvention',
					'webloader.jsNamingConvention',
				),
			),
			'WebLoader\DefaultOutputNamingConvention' => array(
				1 => array(
					'webloader.cssNamingConvention',
					'webloader.jsNamingConvention',
				),
			),
			'WebLoader\IFileCollection' => array(
				1 => array(
					'webloader.cssDefaultFiles',
					'webloader.cssPrintFiles',
					'webloader.jsDefaultFiles',
				),
			),
			'WebLoader\FileCollection' => array(
				1 => array(
					'webloader.cssDefaultFiles',
					'webloader.cssPrintFiles',
					'webloader.jsDefaultFiles',
				),
			),
			'WebLoader\Compiler' => array(
				1 => array(
					'webloader.cssDefaultCompiler',
					'webloader.cssPrintCompiler',
					'webloader.jsDefaultCompiler',
				),
			),
			'WebLoader\LoaderFactory' => array(1 => array('webloader.factory')),
			'IteratorAggregate' => array(1 => array('console.helperSet')),
			'Traversable' => array(1 => array('console.helperSet')),
			'Symfony\Component\Console\Helper\HelperSet' => array(1 => array('console.helperSet')),
			'Symfony\Component\Console\Application' => array(1 => array('console.application')),
			'Kdyby\Console\Application' => array(1 => array('console.application')),
			'Doctrine\Common\Annotations\Reader' => array(
				array('annotations.reflectionReader'),
				array('annotations.reader'),
			),
			'Doctrine\Common\Annotations\AnnotationReader' => array(array('annotations.reflectionReader')),
			'Doctrine\Common\Cache\Cache' => array(
				array(
					'annotations.cache.annotations',
					'doctrine.cache.default.metadata',
					'doctrine.cache.default.query',
					'doctrine.cache.default.ormResult',
					'doctrine.cache.default.hydration',
					'doctrine.cache.default.dbalResult',
				),
			),
			'Doctrine\Common\Persistence\Mapping\Driver\MappingDriver' => array(
				array(
					'doctrine.default.metadataDriver',
					'doctrine.default.driver.App.annotationsImpl',
					'doctrine.default.driver.Kdyby_Doctrine.annotationsImpl',
				),
			),
			'Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain' => array(
				array('doctrine.default.metadataDriver'),
			),
			'Doctrine\ORM\Repository\RepositoryFactory' => array(
				array('doctrine.default.repositoryFactory'),
			),
			'Kdyby\Doctrine\RepositoryFactory' => array(
				array('doctrine.default.repositoryFactory'),
			),
			'Doctrine\ORM\Configuration' => array(
				array('doctrine.default.ormConfiguration'),
			),
			'Doctrine\DBAL\Configuration' => array(
				array(
					'doctrine.default.ormConfiguration',
					'doctrine.default.dbalConfiguration',
				),
			),
			'Kdyby\Doctrine\Configuration' => array(
				array('doctrine.default.ormConfiguration'),
			),
			'Doctrine\Common\EventManager' => array(array('doctrine.default.evm')),
			'Doctrine\ORM\EntityManager' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Doctrine\Common\Persistence\ObjectManager' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Doctrine\ORM\EntityManagerInterface' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Kdyby\Persistence\QueryExecutor' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Kdyby\Persistence\Queryable' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Kdyby\Doctrine\EntityManager' => array(
				1 => array('doctrine.default.entityManager'),
			),
			'Tracy\IBarPanel' => array(
				array('doctrine.default.diagnosticsPanel'),
			),
			'Doctrine\DBAL\Logging\SQLLogger' => array(
				array('doctrine.default.diagnosticsPanel'),
			),
			'Kdyby\Doctrine\Diagnostics\Panel' => array(
				array('doctrine.default.diagnosticsPanel'),
			),
			'Doctrine\DBAL\Connection' => array(
				1 => array('doctrine.default.connection'),
			),
			'Doctrine\DBAL\Driver\Connection' => array(
				1 => array('doctrine.default.connection'),
			),
			'Kdyby\Doctrine\Connection' => array(
				1 => array('doctrine.default.connection'),
			),
			'Kdyby\Doctrine\DI\IRepositoryFactory' => array(
				array(
					'doctrine.repositoryFactory.default.defaultRepositoryFactory',
				),
			),
			'Doctrine\ORM\Tools\SchemaValidator' => array(
				1 => array('doctrine.default.schemaValidator'),
			),
			'Doctrine\ORM\Tools\SchemaTool' => array(
				1 => array('doctrine.default.schemaTool'),
			),
			'Kdyby\Doctrine\Tools\CacheCleaner' => array(
				1 => array('doctrine.default.cacheCleaner'),
			),
			'Doctrine\DBAL\Schema\AbstractSchemaManager' => array(
				1 => array('doctrine.default.schemaManager'),
			),
			'Symfony\Component\Console\Helper\Helper' => array(
				1 => array(
					'doctrine.helper.entityManager',
					'doctrine.helper.connection',
				),
			),
			'Symfony\Component\Console\Helper\HelperInterface' => array(
				1 => array(
					'doctrine.helper.entityManager',
					'doctrine.helper.connection',
				),
			),
			'Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper' => array(
				1 => array('doctrine.helper.entityManager'),
			),
			'Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper' => array(
				1 => array('doctrine.helper.connection'),
			),
			'Symfony\Component\Console\Command\Command' => array(
				array(
					'doctrine.cli.0',
					'doctrine.cli.1',
					'doctrine.cli.2',
					'doctrine.cli.3',
					'doctrine.cli.4',
					'doctrine.cli.5',
					'doctrine.cli.6',
					'doctrine.cli.7',
					'doctrine.cli.8',
					'doctrine.cli.9',
					'doctrine.cli.10',
					'doctrine.cli.11',
					'migrations.continueCommand',
					'migrations.createCommand',
					'migrations.resetCommand',
				),
			),
			'Doctrine\DBAL\Tools\Console\Command\ImportCommand' => array(1 => array('doctrine.cli.0')),
			'Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand' => array(1 => array('doctrine.cli.1')),
			'Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand' => array(1 => array('doctrine.cli.2')),
			'Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand' => array(1 => array('doctrine.cli.3')),
			'Doctrine\ORM\Tools\Console\Command\ConvertMappingCommand' => array(1 => array('doctrine.cli.4')),
			'Kdyby\Doctrine\Console\ConvertMappingCommand' => array(1 => array('doctrine.cli.4')),
			'Doctrine\ORM\Tools\Console\Command\GenerateEntitiesCommand' => array(1 => array('doctrine.cli.5')),
			'Kdyby\Doctrine\Console\GenerateEntitiesCommand' => array(1 => array('doctrine.cli.5')),
			'Doctrine\ORM\Tools\Console\Command\SchemaTool\CreateCommand' => array(1 => array('doctrine.cli.6')),
			'Doctrine\ORM\Tools\Console\Command\SchemaTool\AbstractCommand' => array(
				1 => array(
					'doctrine.cli.6',
					'doctrine.cli.7',
					'doctrine.cli.8',
				),
			),
			'Kdyby\Doctrine\Console\SchemaCreateCommand' => array(1 => array('doctrine.cli.6')),
			'Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand' => array(1 => array('doctrine.cli.7')),
			'Kdyby\Doctrine\Console\SchemaUpdateCommand' => array(1 => array('doctrine.cli.7')),
			'Doctrine\ORM\Tools\Console\Command\SchemaTool\DropCommand' => array(1 => array('doctrine.cli.8')),
			'Kdyby\Doctrine\Console\SchemaDropCommand' => array(1 => array('doctrine.cli.8')),
			'Doctrine\ORM\Tools\Console\Command\GenerateProxiesCommand' => array(1 => array('doctrine.cli.9')),
			'Kdyby\Doctrine\Console\GenerateProxiesCommand' => array(1 => array('doctrine.cli.9')),
			'Doctrine\ORM\Tools\Console\Command\ValidateSchemaCommand' => array(1 => array('doctrine.cli.10')),
			'Kdyby\Doctrine\Console\ValidateSchemaCommand' => array(1 => array('doctrine.cli.10')),
			'Doctrine\ORM\Tools\Console\Command\InfoCommand' => array(1 => array('doctrine.cli.11')),
			'Kdyby\Doctrine\Console\InfoCommand' => array(1 => array('doctrine.cli.11')),
			'Doctrine\Common\Persistence\AbstractManagerRegistry' => array(1 => array('doctrine.registry')),
			'Doctrine\Common\Persistence\ConnectionRegistry' => array(1 => array('doctrine.registry')),
			'Doctrine\Common\Persistence\ManagerRegistry' => array(1 => array('doctrine.registry')),
			'Kdyby\Doctrine\Registry' => array(1 => array('doctrine.registry')),
			'Nextras\Migrations\IDbal' => array(1 => array('migrations.dbal')),
			'Nextras\Migrations\IDriver' => array(1 => array('migrations.driver')),
			'Nextras\Migrations\IExtensionHandler' => array(
				1 => array(
					'migrations.sqlHandler',
					'migrations.phpHandler',
				),
			),
			'Nextras\Migrations\Extensions\SqlHandler' => array(1 => array('migrations.sqlHandler')),
			'Nextras\Migrations\Extensions\PhpHandler' => array(1 => array('migrations.phpHandler')),
			'Nextras\Migrations\Bridges\SymfonyConsole\BaseCommand' => array(
				array(
					'migrations.continueCommand',
					'migrations.createCommand',
					'migrations.resetCommand',
				),
			),
			'Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand' => array(
				1 => array('migrations.continueCommand'),
			),
			'Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand' => array(1 => array('migrations.createCommand')),
			'Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand' => array(1 => array('migrations.resetCommand')),
			'WebLoader\Filter\CssUrlsFilter' => array(1 => array('wlCssFilter')),
			'App\Presenters\BasePresenter' => array(array('application.1', 'application.2')),
			'Nette\Application\UI\Presenter' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\UI\Control' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\UI\PresenterComponent' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\ComponentModel\Container' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\ComponentModel\Component' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\UI\IRenderable' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\ComponentModel\IContainer' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\ComponentModel\IComponent' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\UI\ISignalReceiver' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\UI\IStatePersistent' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'ArrayAccess' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
				),
			),
			'Nette\Application\IPresenter' => array(
				array(
					'application.1',
					'application.2',
					'application.3',
					'application.4',
					'application.5',
				),
			),
			'App\Presenters\ErrorPresenter' => array(array('application.1')),
			'App\Presenters\HomePresenter' => array(array('application.2')),
			'KdybyModule\CliPresenter' => array(array('application.3')),
			'NetteModule\ErrorPresenter' => array(array('application.4')),
			'NetteModule\MicroPresenter' => array(array('application.5')),
			'Nette\DI\Container' => array(1 => array('container')),
		),
		'services' => array(
			'annotations.cache.annotations' => 'Doctrine\Common\Cache\Cache',
			'annotations.reader' => 'Doctrine\Common\Annotations\Reader',
			'annotations.reflectionReader' => 'Doctrine\Common\Annotations\AnnotationReader',
			'application.1' => 'App\Presenters\ErrorPresenter',
			'application.2' => 'App\Presenters\HomePresenter',
			'application.3' => 'KdybyModule\CliPresenter',
			'application.4' => 'NetteModule\ErrorPresenter',
			'application.5' => 'NetteModule\MicroPresenter',
			'application.application' => 'Nette\Application\Application',
			'application.linkGenerator' => 'Nette\Application\LinkGenerator',
			'application.presenterFactory' => 'Nette\Application\IPresenterFactory',
			'cache.journal' => 'Nette\Caching\Storages\IJournal',
			'cache.storage' => 'Nette\Caching\IStorage',
			'console.application' => 'Kdyby\Console\Application',
			'console.helperSet' => 'Symfony\Component\Console\Helper\HelperSet',
			'container' => 'Nette\DI\Container',
			'database.default.connection' => 'Nette\Database\Connection',
			'database.default.context' => 'Nette\Database\Context',
			'database.default.conventions' => 'Nette\Database\Conventions\DiscoveredConventions',
			'database.default.structure' => 'Nette\Database\Structure',
			'doctrine.cache.default.dbalResult' => 'Doctrine\Common\Cache\Cache',
			'doctrine.cache.default.hydration' => 'Doctrine\Common\Cache\Cache',
			'doctrine.cache.default.metadata' => 'Doctrine\Common\Cache\Cache',
			'doctrine.cache.default.ormResult' => 'Doctrine\Common\Cache\Cache',
			'doctrine.cache.default.query' => 'Doctrine\Common\Cache\Cache',
			'doctrine.cli.0' => 'Doctrine\DBAL\Tools\Console\Command\ImportCommand',
			'doctrine.cli.1' => 'Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand',
			'doctrine.cli.10' => 'Kdyby\Doctrine\Console\ValidateSchemaCommand',
			'doctrine.cli.11' => 'Kdyby\Doctrine\Console\InfoCommand',
			'doctrine.cli.2' => 'Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand',
			'doctrine.cli.3' => 'Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand',
			'doctrine.cli.4' => 'Kdyby\Doctrine\Console\ConvertMappingCommand',
			'doctrine.cli.5' => 'Kdyby\Doctrine\Console\GenerateEntitiesCommand',
			'doctrine.cli.6' => 'Kdyby\Doctrine\Console\SchemaCreateCommand',
			'doctrine.cli.7' => 'Kdyby\Doctrine\Console\SchemaUpdateCommand',
			'doctrine.cli.8' => 'Kdyby\Doctrine\Console\SchemaDropCommand',
			'doctrine.cli.9' => 'Kdyby\Doctrine\Console\GenerateProxiesCommand',
			'doctrine.default.cacheCleaner' => 'Kdyby\Doctrine\Tools\CacheCleaner',
			'doctrine.default.connection' => 'Kdyby\Doctrine\Connection',
			'doctrine.default.dbalConfiguration' => 'Doctrine\DBAL\Configuration',
			'doctrine.default.diagnosticsPanel' => 'Kdyby\Doctrine\Diagnostics\Panel',
			'doctrine.default.driver.App.annotationsImpl' => 'Doctrine\Common\Persistence\Mapping\Driver\MappingDriver',
			'doctrine.default.driver.Kdyby_Doctrine.annotationsImpl' => 'Doctrine\Common\Persistence\Mapping\Driver\MappingDriver',
			'doctrine.default.entityManager' => 'Kdyby\Doctrine\EntityManager',
			'doctrine.default.evm' => 'Doctrine\Common\EventManager',
			'doctrine.default.metadataDriver' => 'Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain',
			'doctrine.default.ormConfiguration' => 'Kdyby\Doctrine\Configuration',
			'doctrine.default.repositoryFactory' => 'Kdyby\Doctrine\RepositoryFactory',
			'doctrine.default.schemaManager' => 'Doctrine\DBAL\Schema\AbstractSchemaManager',
			'doctrine.default.schemaTool' => 'Doctrine\ORM\Tools\SchemaTool',
			'doctrine.default.schemaValidator' => 'Doctrine\ORM\Tools\SchemaValidator',
			'doctrine.helper.connection' => 'Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper',
			'doctrine.helper.entityManager' => 'Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper',
			'doctrine.registry' => 'Kdyby\Doctrine\Registry',
			'doctrine.repositoryFactory.default.defaultRepositoryFactory' => 'Kdyby\Doctrine\EntityRepository',
			'http.context' => 'Nette\Http\Context',
			'http.request' => 'Nette\Http\Request',
			'http.requestFactory' => 'Nette\Http\RequestFactory',
			'http.response' => 'Nette\Http\Response',
			'latte.latteFactory' => 'Latte\Engine',
			'latte.templateFactory' => 'Nette\Application\UI\ITemplateFactory',
			'mail.mailer' => 'Nette\Mail\IMailer',
			'migrations.continueCommand' => 'Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand',
			'migrations.createCommand' => 'Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand',
			'migrations.dbal' => 'Nextras\Migrations\IDbal',
			'migrations.driver' => 'Nextras\Migrations\IDriver',
			'migrations.phpHandler' => 'Nextras\Migrations\Extensions\PhpHandler',
			'migrations.resetCommand' => 'Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand',
			'migrations.sqlHandler' => 'Nextras\Migrations\Extensions\SqlHandler',
			'nette.latte' => 'Latte\Engine',
			'nette.template' => 'Nette\Templating\FileTemplate',
			'routing.router' => 'Nette\Application\IRouter',
			'security.user' => 'Nette\Security\User',
			'security.userStorage' => 'Nette\Security\IUserStorage',
			'session.session' => 'Nette\Http\Session',
			'tracy.bar' => 'Tracy\Bar',
			'tracy.blueScreen' => 'Tracy\BlueScreen',
			'tracy.logger' => 'Tracy\ILogger',
			'webloader.cssDefaultCompiler' => 'WebLoader\Compiler',
			'webloader.cssDefaultFiles' => 'WebLoader\FileCollection',
			'webloader.cssNamingConvention' => 'WebLoader\DefaultOutputNamingConvention',
			'webloader.cssPrintCompiler' => 'WebLoader\Compiler',
			'webloader.cssPrintFiles' => 'WebLoader\FileCollection',
			'webloader.factory' => 'WebLoader\LoaderFactory',
			'webloader.jsDefaultCompiler' => 'WebLoader\Compiler',
			'webloader.jsDefaultFiles' => 'WebLoader\FileCollection',
			'webloader.jsNamingConvention' => 'WebLoader\DefaultOutputNamingConvention',
			'wlCssFilter' => 'WebLoader\Filter\CssUrlsFilter',
		),
		'tags' => array(
			'inject' => array(
				'annotations.cache.annotations' => FALSE,
				'application.1' => TRUE,
				'application.2' => TRUE,
				'application.3' => TRUE,
				'application.4' => TRUE,
				'application.5' => TRUE,
				'console.application' => FALSE,
				'console.helperSet' => FALSE,
				'doctrine.cache.default.dbalResult' => FALSE,
				'doctrine.cache.default.hydration' => FALSE,
				'doctrine.cache.default.metadata' => FALSE,
				'doctrine.cache.default.ormResult' => FALSE,
				'doctrine.cache.default.query' => FALSE,
				'doctrine.cli.0' => FALSE,
				'doctrine.cli.1' => FALSE,
				'doctrine.cli.10' => FALSE,
				'doctrine.cli.11' => FALSE,
				'doctrine.cli.2' => FALSE,
				'doctrine.cli.3' => FALSE,
				'doctrine.cli.4' => FALSE,
				'doctrine.cli.5' => FALSE,
				'doctrine.cli.6' => FALSE,
				'doctrine.cli.7' => FALSE,
				'doctrine.cli.8' => FALSE,
				'doctrine.cli.9' => FALSE,
				'doctrine.default.connection' => FALSE,
				'doctrine.default.dbalConfiguration' => FALSE,
				'doctrine.default.driver.App.annotationsImpl' => FALSE,
				'doctrine.default.driver.Kdyby_Doctrine.annotationsImpl' => FALSE,
				'doctrine.default.entityManager' => FALSE,
				'doctrine.default.metadataDriver' => FALSE,
				'doctrine.default.ormConfiguration' => FALSE,
			),
			'nette.presenter' => array(
				'application.1' => 'App\Presenters\ErrorPresenter',
				'application.2' => 'App\Presenters\HomePresenter',
				'application.3' => 'KdybyModule\CliPresenter',
				'application.4' => 'NetteModule\ErrorPresenter',
				'application.5' => 'NetteModule\MicroPresenter',
			),
			'kdyby.console.command' => array(
				'doctrine.cli.0' => TRUE,
				'doctrine.cli.1' => TRUE,
				'doctrine.cli.10' => TRUE,
				'doctrine.cli.11' => TRUE,
				'doctrine.cli.2' => TRUE,
				'doctrine.cli.3' => TRUE,
				'doctrine.cli.4' => TRUE,
				'doctrine.cli.5' => TRUE,
				'doctrine.cli.6' => TRUE,
				'doctrine.cli.7' => TRUE,
				'doctrine.cli.8' => TRUE,
				'doctrine.cli.9' => TRUE,
				'migrations.continueCommand' => TRUE,
				'migrations.createCommand' => TRUE,
				'migrations.resetCommand' => TRUE,
			),
			'doctrine.connection' => array('doctrine.default.connection' => TRUE),
			'kdyby.doctrine.connection' => array('doctrine.default.connection' => TRUE),
			'doctrine.entityManager' => array(
				'doctrine.default.entityManager' => TRUE,
			),
			'kdyby.doctrine.entityManager' => array(
				'doctrine.default.entityManager' => TRUE,
			),
			'kdyby.console.helper' => array(
				'doctrine.helper.connection' => 'db',
				'doctrine.helper.entityManager' => 'em',
			),
		),
		'aliases' => array(
			'application' => 'application.application',
			'cacheStorage' => 'cache.storage',
			'database.default' => 'database.default.connection',
			'doctrine.cacheCleaner' => 'doctrine.default.cacheCleaner',
			'doctrine.schemaManager' => 'doctrine.default.schemaManager',
			'doctrine.schemaTool' => 'doctrine.default.schemaTool',
			'doctrine.schemaValidator' => 'doctrine.default.schemaValidator',
			'httpRequest' => 'http.request',
			'httpResponse' => 'http.response',
			'nette.cacheJournal' => 'cache.journal',
			'nette.database.default' => 'database.default',
			'nette.database.default.context' => 'database.default.context',
			'nette.httpContext' => 'http.context',
			'nette.httpRequestFactory' => 'http.requestFactory',
			'nette.latteFactory' => 'latte.latteFactory',
			'nette.mailer' => 'mail.mailer',
			'nette.presenterFactory' => 'application.presenterFactory',
			'nette.templateFactory' => 'latte.templateFactory',
			'nette.userStorage' => 'security.userStorage',
			'router' => 'routing.router',
			'session' => 'session.session',
			'user' => 'security.user',
		),
	);


	public function __construct()
	{
		parent::__construct(array(
			'appDir' => '/var/www/tccl/app',
			'wwwDir' => '/var/www/tccl/www',
			'debugMode' => TRUE,
			'productionMode' => FALSE,
			'environment' => 'development',
			'consoleMode' => FALSE,
			'container' => array('class' => NULL, 'parent' => NULL),
			'tempDir' => '/var/www/tccl/app/../temp',
			'emails' => array(
				'infobot' => 'Infobot <noreply@021a.eu>',
				'contact' => 'Contact <info@021a.eu>',
			),
			'basePath' => '/',
			'webtempdir' => '/var/www/tccl/www/webtemp',
			'bowerdir' => '/var/www/tccl/www/bower',
			'vendordir' => '/vendor',
			'wwwdir' => '/var/www/tccl/www',
			'webloader' => array(
				'jsDefaults' => array(
					'sourceDir' => '/var/www/tccl/www/js',
					'tempDir' => '/var/www/tccl/www/webtemp',
					'tempPath' => 'webtemp',
					'files' => array(),
					'remoteFiles' => array(),
					'filters' => array(),
					'fileFilters' => array(),
					'joinFiles' => TRUE,
					'namingConvention' => '@webloader.jsNamingConvention',
				),
				'cssDefaults' => array(
					'sourceDir' => '/var/www/tccl/www/css',
					'tempDir' => '/var/www/tccl/www/webtemp',
					'tempPath' => 'webtemp',
					'files' => array(),
					'remoteFiles' => array(),
					'filters' => array(),
					'fileFilters' => array(),
					'joinFiles' => TRUE,
					'namingConvention' => '@webloader.cssNamingConvention',
				),
				'js' => array(
					'default' => array(
						'tempDir' => '/var/www/tccl/www/webtemp',
						'files' => array(
							'/var/www/tccl/www/bower/jquery/jquery.min.js',
							'/var/www/tccl/www/bower/bootstrap/dist/js/bootstrap.min.js',
							'/var/www/tccl/www/js/lib/bootstrap-check-buttons.js',
							'/var/www/tccl/www/bower/nette.ajax.js/nette.ajax.js',
							'/var/www/tccl/www/js/nextras.datagrid.js',
							'/var/www/tccl/www/js/netteForms.js',
							'/var/www/tccl/www/js/confirm.dialog.js',
							'/var/www/tccl/www/js/main.js',
						),
					),
				),
				'css' => array(
					'default' => array(
						'fileFilters' => array('@wlCssFilter'),
						'tempDir' => '/var/www/tccl/www/webtemp',
						'files' => array(
							'/var/www/tccl/www/bower/bootstrap/dist/css/bootstrap.css',
							'/var/www/tccl/www/bower/fontawesome/css/font-awesome.css',
							'/var/www/tccl/www/css/bootstrap.css',
							'/var/www/tccl/www/css/main.css',
						),
					),
					'print' => array('/var/www/tccl/www/css/print.css'),
				),
			),
			'doctrine.debug' => FALSE,
			'doctrine' => array(
				'orm' => array('defaultEntityManager' => 'default'),
				'dbal' => array('defaultConnection' => 'default'),
			),
		));
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceAnnotations__cache__annotations()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Annotations', TRUE);
		$service->setNamespace('Kdyby_annotations.cache.annotations_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\Common\Annotations\Reader
	 */
	public function createServiceAnnotations__reader()
	{
		$service = new Doctrine\Common\Annotations\CachedReader($this->getService('annotations.reflectionReader'), $this->getService('annotations.cache.annotations'),
			TRUE);
		return $service;
	}


	/**
	 * @return Doctrine\Common\Annotations\AnnotationReader
	 */
	public function createServiceAnnotations__reflectionReader()
	{
		$service = new Doctrine\Common\Annotations\AnnotationReader;
		$service->addGlobalIgnoredName('persistent');
		$service->addGlobalIgnoredName('serializationVersion');
		return $service;
	}


	/**
	 * @return App\Presenters\ErrorPresenter
	 */
	public function createServiceApplication__1()
	{
		$service = new App\Presenters\ErrorPresenter($this->getService('tracy.logger'));
		$service->injectPrimary($this, $this->getService('application.presenterFactory'), $this->getService('routing.router'),
			$this->getService('http.request'), $this->getService('http.response'), $this->getService('session.session'),
			$this->getService('security.user'), $this->getService('latte.templateFactory'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	/**
	 * @return App\Presenters\HomePresenter
	 */
	public function createServiceApplication__2()
	{
		$service = new App\Presenters\HomePresenter;
		$service->injectPrimary($this, $this->getService('application.presenterFactory'), $this->getService('routing.router'),
			$this->getService('http.request'), $this->getService('http.response'), $this->getService('session.session'),
			$this->getService('security.user'), $this->getService('latte.templateFactory'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	/**
	 * @return KdybyModule\CliPresenter
	 */
	public function createServiceApplication__3()
	{
		$service = new KdybyModule\CliPresenter;
		$service->injectPrimary($this, $this->getService('application.presenterFactory'), $this->getService('routing.router'),
			$this->getService('http.request'), $this->getService('http.response'), $this->getService('session.session'),
			$this->getService('security.user'), $this->getService('latte.templateFactory'));
		$service->injectConsole($this->getService('console.application'));
		$service->invalidLinkMode = 5;
		return $service;
	}


	/**
	 * @return NetteModule\ErrorPresenter
	 */
	public function createServiceApplication__4()
	{
		$service = new NetteModule\ErrorPresenter($this->getService('tracy.logger'));
		return $service;
	}


	/**
	 * @return NetteModule\MicroPresenter
	 */
	public function createServiceApplication__5()
	{
		$service = new NetteModule\MicroPresenter($this, $this->getService('http.request'), $this->getService('routing.router'));
		return $service;
	}


	/**
	 * @return Nette\Application\Application
	 */
	public function createServiceApplication__application()
	{
		$service = new Nette\Application\Application($this->getService('application.presenterFactory'), $this->getService('routing.router'),
			$this->getService('http.request'), $this->getService('http.response'));
		$service->catchExceptions = FALSE;
		$service->errorPresenter = 'Error';
		Nette\Bridges\ApplicationTracy\RoutingPanel::initializePanel($service);
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\ApplicationTracy\RoutingPanel($this->getService('routing.router'), $this->getService('http.request'),
			$this->getService('application.presenterFactory')));
		return $service;
	}


	/**
	 * @return Nette\Application\LinkGenerator
	 */
	public function createServiceApplication__linkGenerator()
	{
		$service = new Nette\Application\LinkGenerator($this->getService('routing.router'), $this->getService('http.request')->getUrl(),
			$this->getService('application.presenterFactory'));
		return $service;
	}


	/**
	 * @return Nette\Application\IPresenterFactory
	 */
	public function createServiceApplication__presenterFactory()
	{
		$service = new Nette\Application\PresenterFactory(new Nette\Bridges\ApplicationDI\PresenterFactoryCallback($this, 5, '/var/www/tccl/app/../temp/cache/Nette%5CBridges%5CApplicationDI%5CApplicationExtension'));
		$service->setMapping(array('*' => 'App\Presenters\*Presenter'));
		return $service;
	}


	/**
	 * @return Nette\Caching\Storages\IJournal
	 */
	public function createServiceCache__journal()
	{
		$service = new Nette\Caching\Storages\FileJournal('/var/www/tccl/app/../temp');
		return $service;
	}


	/**
	 * @return Nette\Caching\IStorage
	 */
	public function createServiceCache__storage()
	{
		$service = new Nette\Caching\Storages\FileStorage('/var/www/tccl/app/../temp/cache', $this->getService('cache.journal'));
		return $service;
	}


	/**
	 * @return Kdyby\Console\Application
	 */
	public function createServiceConsole__application()
	{
		$service = new Kdyby\Console\Application('Nette Framework', '2.3.10');
		$service->setHelperSet($this->getService('console.helperSet'));
		$service->injectServiceLocator($this);
		return $service;
	}


	/**
	 * @return Symfony\Component\Console\Helper\HelperSet
	 */
	public function createServiceConsole__helperSet()
	{
		$service = new Symfony\Component\Console\Helper\HelperSet;
		$service->set(new Symfony\Component\Console\Helper\ProcessHelper);
		$service->set(new Symfony\Component\Console\Helper\DescriptorHelper);
		$service->set(new Symfony\Component\Console\Helper\FormatterHelper);
		$service->set(new Symfony\Component\Console\Helper\QuestionHelper);
		$service->set(new Symfony\Component\Console\Helper\DebugFormatterHelper);
		$service->set(new Kdyby\Console\Helpers\PresenterHelper($this->getService('application.application')));
		$service->set(new Kdyby\Console\ContainerHelper($this), 'dic');
		return $service;
	}


	/**
	 * @return Nette\DI\Container
	 */
	public function createServiceContainer()
	{
		return $this;
	}


	/**
	 * @return Nette\Database\Connection
	 */
	public function createServiceDatabase__default__connection()
	{
		$service = new Nette\Database\Connection('pgsql:host=127.0.0.1;dbname=tccl;port=5432', 'postgres', 'postgres', array('lazy' => TRUE));
		$this->getService('tracy.blueScreen')->addPanel('Nette\Bridges\DatabaseTracy\ConnectionPanel::renderException');
		Nette\Database\Helpers::createDebugPanel($service, TRUE, 'default');
		return $service;
	}


	/**
	 * @return Nette\Database\Context
	 */
	public function createServiceDatabase__default__context()
	{
		$service = new Nette\Database\Context($this->getService('database.default.connection'), $this->getService('database.default.structure'),
			$this->getService('database.default.conventions'), $this->getService('cache.storage'));
		return $service;
	}


	/**
	 * @return Nette\Database\Conventions\DiscoveredConventions
	 */
	public function createServiceDatabase__default__conventions()
	{
		$service = new Nette\Database\Conventions\DiscoveredConventions($this->getService('database.default.structure'));
		return $service;
	}


	/**
	 * @return Nette\Database\Structure
	 */
	public function createServiceDatabase__default__structure()
	{
		$service = new Nette\Database\Structure($this->getService('database.default.connection'), $this->getService('cache.storage'));
		return $service;
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceDoctrine__cache__default__dbalResult()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Default.dbalResult', FALSE);
		$service->setNamespace('Kdyby_doctrine.cache.default.dbalResult_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceDoctrine__cache__default__hydration()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Default.hydration', FALSE);
		$service->setNamespace('Kdyby_doctrine.cache.default.hydration_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceDoctrine__cache__default__metadata()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Default.metadata', FALSE);
		$service->setNamespace('Kdyby_doctrine.cache.default.metadata_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceDoctrine__cache__default__ormResult()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Default.ormResult', FALSE);
		$service->setNamespace('Kdyby_doctrine.cache.default.ormResult_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\Common\Cache\Cache
	 */
	public function createServiceDoctrine__cache__default__query()
	{
		$service = new Kdyby\DoctrineCache\Cache($this->getService('cache.storage'), 'Doctrine.Default.query', FALSE);
		$service->setNamespace('Kdyby_doctrine.cache.default.query_abbe39ec');
		return $service;
	}


	/**
	 * @return Doctrine\DBAL\Tools\Console\Command\ImportCommand
	 */
	public function createServiceDoctrine__cli__0()
	{
		$service = new Doctrine\DBAL\Tools\Console\Command\ImportCommand;
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand
	 */
	public function createServiceDoctrine__cli__1()
	{
		$service = new Doctrine\ORM\Tools\Console\Command\ClearCache\MetadataCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\ValidateSchemaCommand
	 */
	public function createServiceDoctrine__cli__10()
	{
		$service = new Kdyby\Doctrine\Console\ValidateSchemaCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\InfoCommand
	 */
	public function createServiceDoctrine__cli__11()
	{
		$service = new Kdyby\Doctrine\Console\InfoCommand;
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand
	 */
	public function createServiceDoctrine__cli__2()
	{
		$service = new Doctrine\ORM\Tools\Console\Command\ClearCache\ResultCommand;
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand
	 */
	public function createServiceDoctrine__cli__3()
	{
		$service = new Doctrine\ORM\Tools\Console\Command\ClearCache\QueryCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\ConvertMappingCommand
	 */
	public function createServiceDoctrine__cli__4()
	{
		$service = new Kdyby\Doctrine\Console\ConvertMappingCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\GenerateEntitiesCommand
	 */
	public function createServiceDoctrine__cli__5()
	{
		$service = new Kdyby\Doctrine\Console\GenerateEntitiesCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\SchemaCreateCommand
	 */
	public function createServiceDoctrine__cli__6()
	{
		$service = new Kdyby\Doctrine\Console\SchemaCreateCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\SchemaUpdateCommand
	 */
	public function createServiceDoctrine__cli__7()
	{
		$service = new Kdyby\Doctrine\Console\SchemaUpdateCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\SchemaDropCommand
	 */
	public function createServiceDoctrine__cli__8()
	{
		$service = new Kdyby\Doctrine\Console\SchemaDropCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Console\GenerateProxiesCommand
	 */
	public function createServiceDoctrine__cli__9()
	{
		$service = new Kdyby\Doctrine\Console\GenerateProxiesCommand;
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Tools\CacheCleaner
	 */
	public function createServiceDoctrine__default__cacheCleaner()
	{
		$service = new Kdyby\Doctrine\Tools\CacheCleaner($this->getService('doctrine.default.entityManager'));
		$service->addCacheStorage($this->getService('annotations.cache.annotations'));
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Connection
	 */
	public function createServiceDoctrine__default__connection()
	{
		$service = Kdyby\Doctrine\Connection::create(array(
			'dbname' => 'tccl',
			'host' => '127.0.0.1',
			'port' => 5432,
			'user' => 'postgres',
			'password' => 'postgres',
			'charset' => 'UTF8',
			'driver' => 'pdo_pgsql',
			'driverClass' => NULL,
			'options' => NULL,
			'path' => NULL,
			'memory' => NULL,
			'unix_socket' => NULL,
			'platformService' => NULL,
			'defaultTableOptions' => array(),
			'schemaFilter' => NULL,
			'debug' => TRUE,
		), $this->getService('doctrine.default.dbalConfiguration'), $this->getService('doctrine.default.evm'));
		if (!$service instanceof Kdyby\Doctrine\Connection) {
			throw new Nette\UnexpectedValueException('Unable to create service \'doctrine.default.connection\', value returned by factory is not Kdyby\Doctrine\Connection type.');
		}
		$service->setSchemaTypes(array());
		$service->setDbalTypes(array());
		$panel = $this->getService('doctrine.default.diagnosticsPanel')->bindConnection($service);
		$panel->enableLogging();
		return $service;
	}


	/**
	 * @return Doctrine\DBAL\Configuration
	 */
	public function createServiceDoctrine__default__dbalConfiguration()
	{
		$service = new Doctrine\DBAL\Configuration;
		$service->setResultCacheImpl($this->getService('doctrine.cache.default.dbalResult'));
		$service->setSQLLogger(new Doctrine\DBAL\Logging\LoggerChain);
		$service->setFilterSchemaAssetsExpression(NULL);
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Diagnostics\Panel
	 */
	public function createServiceDoctrine__default__diagnosticsPanel()
	{
		$service = new Kdyby\Doctrine\Diagnostics\Panel;
		return $service;
	}


	/**
	 * @return Doctrine\Common\Persistence\Mapping\Driver\MappingDriver
	 */
	public function createServiceDoctrine__default__driver__App__annotationsImpl()
	{
		$service = new Kdyby\Doctrine\Mapping\AnnotationDriver(array('/var/www/tccl/app'), $this->getService('annotations.reader'),
			$this->getService('doctrine.cache.default.metadata'));
		return $service;
	}


	/**
	 * @return Doctrine\Common\Persistence\Mapping\Driver\MappingDriver
	 */
	public function createServiceDoctrine__default__driver__Kdyby_Doctrine__annotationsImpl()
	{
		$service = new Kdyby\Doctrine\Mapping\AnnotationDriver(array(
			'/var/www/tccl/vendor/kdyby/doctrine/src/Kdyby/Doctrine/DI/../Entities',
		), $this->getService('annotations.reader'), $this->getService('doctrine.cache.default.metadata'));
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\EntityManager
	 */
	public function createServiceDoctrine__default__entityManager()
	{
		$service = Kdyby\Doctrine\EntityManager::create($this->getService('doctrine.default.connection'), $this->getService('doctrine.default.ormConfiguration'),
			$this->getService('doctrine.default.evm'));
		if (!$service instanceof Kdyby\Doctrine\EntityManager) {
			throw new Nette\UnexpectedValueException('Unable to create service \'doctrine.default.entityManager\', value returned by factory is not Kdyby\Doctrine\EntityManager type.');
		}
		$this->getService('doctrine.default.diagnosticsPanel')->bindEntityManager($service);
		return $service;
	}


	/**
	 * @return Doctrine\Common\EventManager
	 */
	public function createServiceDoctrine__default__evm()
	{
		$service = new Doctrine\Common\EventManager;
		return $service;
	}


	/**
	 * @return Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain
	 */
	public function createServiceDoctrine__default__metadataDriver()
	{
		$service = new Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
		$service->addDriver($this->getService('doctrine.default.driver.App.annotationsImpl'), 'App');
		$service->addDriver($this->getService('doctrine.default.driver.Kdyby_Doctrine.annotationsImpl'), 'Kdyby\Doctrine');
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Configuration
	 */
	public function createServiceDoctrine__default__ormConfiguration()
	{
		$service = new Kdyby\Doctrine\Configuration;
		$service->setMetadataCacheImpl($this->getService('doctrine.cache.default.metadata'));
		$service->setQueryCacheImpl($this->getService('doctrine.cache.default.query'));
		$service->setResultCacheImpl($this->getService('doctrine.cache.default.ormResult'));
		$service->setHydrationCacheImpl($this->getService('doctrine.cache.default.hydration'));
		$service->setMetadataDriverImpl($this->getService('doctrine.default.metadataDriver'));
		$service->setClassMetadataFactoryName('Kdyby\Doctrine\Mapping\ClassMetadataFactory');
		$service->setDefaultRepositoryClassName('Kdyby\Doctrine\EntityRepository');
		$service->setQueryBuilderClassName('Kdyby\Doctrine\QueryBuilder');
		$service->setRepositoryFactory($this->getService('doctrine.default.repositoryFactory'));
		$service->setProxyDir('/var/www/tccl/app/../temp/proxies');
		$service->setProxyNamespace('Kdyby\GeneratedProxy');
		$service->setAutoGenerateProxyClasses(1);
		$service->setEntityNamespaces(array());
		$service->setCustomHydrationModes(array());
		$service->setCustomStringFunctions(array());
		$service->setCustomNumericFunctions(array());
		$service->setCustomDatetimeFunctions(array());
		$service->setDefaultQueryHints(array());
		$service->setNamingStrategy(new Doctrine\ORM\Mapping\UnderscoreNamingStrategy);
		$service->setQuoteStrategy(new Doctrine\ORM\Mapping\DefaultQuoteStrategy);
		$service->setEntityListenerResolver(new Kdyby\Doctrine\Mapping\EntityListenerResolver($this));
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\RepositoryFactory
	 */
	public function createServiceDoctrine__default__repositoryFactory()
	{
		$service = new Kdyby\Doctrine\RepositoryFactory($this);
		$service->setServiceIdsMap(array(), 'doctrine.repositoryFactory.default.defaultRepositoryFactory');
		return $service;
	}


	/**
	 * @return Doctrine\DBAL\Schema\AbstractSchemaManager
	 */
	public function createServiceDoctrine__default__schemaManager()
	{
		$service = $this->getService('doctrine.default.connection')->getSchemaManager();
		if (!$service instanceof Doctrine\DBAL\Schema\AbstractSchemaManager) {
			throw new Nette\UnexpectedValueException('Unable to create service \'doctrine.default.schemaManager\', value returned by factory is not Doctrine\DBAL\Schema\AbstractSchemaManager type.');
		}
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\SchemaTool
	 */
	public function createServiceDoctrine__default__schemaTool()
	{
		$service = new Doctrine\ORM\Tools\SchemaTool($this->getService('doctrine.default.entityManager'));
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\SchemaValidator
	 */
	public function createServiceDoctrine__default__schemaValidator()
	{
		$service = new Doctrine\ORM\Tools\SchemaValidator($this->getService('doctrine.default.entityManager'));
		return $service;
	}


	/**
	 * @return Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper
	 */
	public function createServiceDoctrine__helper__connection()
	{
		$service = new Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($this->getService('doctrine.default.connection'));
		return $service;
	}


	/**
	 * @return Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper
	 */
	public function createServiceDoctrine__helper__entityManager()
	{
		$service = new Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($this->getService('doctrine.default.entityManager'));
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\Registry
	 */
	public function createServiceDoctrine__registry()
	{
		$service = new Kdyby\Doctrine\Registry(array(
			'default' => 'doctrine.default.connection',
		), array(
			'default' => 'doctrine.default.entityManager',
		), 'default', 'default', $this);
		return $service;
	}


	/**
	 * @return Kdyby\Doctrine\DI\IRepositoryFactory
	 */
	public function createServiceDoctrine__repositoryFactory__default__defaultRepositoryFactory()
	{
		return new Container_fdc70f1db7_Kdyby_Doctrine_DI_IRepositoryFactoryImpl_doctrine_repositoryFactory_default_defaultRepositoryFactory($this);
	}


	/**
	 * @return Nette\Http\Context
	 */
	public function createServiceHttp__context()
	{
		$service = new Nette\Http\Context($this->getService('http.request'), $this->getService('http.response'));
		return $service;
	}


	/**
	 * @return Nette\Http\Request
	 */
	public function createServiceHttp__request()
	{
		$service = $this->getService('http.requestFactory')->createHttpRequest();
		if (!$service instanceof Nette\Http\Request) {
			throw new Nette\UnexpectedValueException('Unable to create service \'http.request\', value returned by factory is not Nette\Http\Request type.');
		}
		return $service;
	}


	/**
	 * @return Nette\Http\RequestFactory
	 */
	public function createServiceHttp__requestFactory()
	{
		$service = new Nette\Http\RequestFactory;
		$service->setProxy(array());
		return $service;
	}


	/**
	 * @return Nette\Http\Response
	 */
	public function createServiceHttp__response()
	{
		$service = new Nette\Http\Response;
		return $service;
	}


	/**
	 * @return Nette\Bridges\ApplicationLatte\ILatteFactory
	 */
	public function createServiceLatte__latteFactory()
	{
		return new Container_fdc70f1db7_Nette_Bridges_ApplicationLatte_ILatteFactoryImpl_latte_latteFactory($this);
	}


	/**
	 * @return Nette\Application\UI\ITemplateFactory
	 */
	public function createServiceLatte__templateFactory()
	{
		$service = new Nette\Bridges\ApplicationLatte\TemplateFactory($this->getService('latte.latteFactory'), $this->getService('http.request'),
			$this->getService('http.response'), $this->getService('security.user'), $this->getService('cache.storage'));
		return $service;
	}


	/**
	 * @return Nette\Mail\IMailer
	 */
	public function createServiceMail__mailer()
	{
		$service = new Nette\Mail\SendmailMailer;
		return $service;
	}


	/**
	 * @return Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand
	 */
	public function createServiceMigrations__continueCommand()
	{
		$service = new Nextras\Migrations\Bridges\SymfonyConsole\ContinueCommand($this->getService('migrations.driver'), '/var/www/tccl/app/../migrations',
			array(
			'sql' => $this->getService('migrations.sqlHandler'),
			'php' => $this->getService('migrations.phpHandler'),
		));
		return $service;
	}


	/**
	 * @return Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand
	 */
	public function createServiceMigrations__createCommand()
	{
		$service = new Nextras\Migrations\Bridges\SymfonyConsole\CreateCommand($this->getService('migrations.driver'), '/var/www/tccl/app/../migrations',
			array(
			'sql' => $this->getService('migrations.sqlHandler'),
			'php' => $this->getService('migrations.phpHandler'),
		));
		return $service;
	}


	/**
	 * @return Nextras\Migrations\IDbal
	 */
	public function createServiceMigrations__dbal()
	{
		$service = new Nextras\Migrations\Bridges\DoctrineDbal\DoctrineAdapter($this->getService('doctrine.default.connection'));
		return $service;
	}


	/**
	 * @return Nextras\Migrations\IDriver
	 */
	public function createServiceMigrations__driver()
	{
		$service = new Nextras\Migrations\Drivers\PgSqlDriver($this->getService('migrations.dbal'));
		return $service;
	}


	/**
	 * @return Nextras\Migrations\Extensions\PhpHandler
	 */
	public function createServiceMigrations__phpHandler()
	{
		$service = new Nextras\Migrations\Extensions\PhpHandler;
		return $service;
	}


	/**
	 * @return Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand
	 */
	public function createServiceMigrations__resetCommand()
	{
		$service = new Nextras\Migrations\Bridges\SymfonyConsole\ResetCommand($this->getService('migrations.driver'), '/var/www/tccl/app/../migrations',
			array(
			'sql' => $this->getService('migrations.sqlHandler'),
			'php' => $this->getService('migrations.phpHandler'),
		));
		return $service;
	}


	/**
	 * @return Nextras\Migrations\Extensions\SqlHandler
	 */
	public function createServiceMigrations__sqlHandler()
	{
		$service = new Nextras\Migrations\Extensions\SqlHandler($this->getService('migrations.driver'));
		return $service;
	}


	/**
	 * @return Latte\Engine
	 */
	public function createServiceNette__latte()
	{
		$service = new Latte\Engine;
		trigger_error('Service nette.latte is deprecated, implement Nette\Bridges\ApplicationLatte\ILatteFactory.',
			16384);
		$service->setTempDirectory('/var/www/tccl/app/../temp/cache/latte');
		$service->setAutoRefresh(TRUE);
		$service->setContentType('html');
		Nette\Utils\Html::$xhtml = FALSE;
		$service->onCompile[] = function ($engine) { Nextras\Latte\Macros\RedefineMacro::install($engine->getCompiler()); };
		$service->onCompile[] = function ($engine) { Instante\Bootstrap3Renderer\Latte\FormMacros::install($engine->getCompiler()); };
		return $service;
	}


	/**
	 * @return Nette\Templating\FileTemplate
	 */
	public function createServiceNette__template()
	{
		$service = new Nette\Templating\FileTemplate;
		trigger_error('Service nette.template is deprecated.', 16384);
		$service->registerFilter($this->getService('latte.latteFactory')->create());
		$service->registerHelperLoader('Nette\Templating\Helpers::loader');
		return $service;
	}


	/**
	 * @return Nette\Application\IRouter
	 */
	public function createServiceRouting__router()
	{
		$service = App\RouterFactory::createRouter();
		if (!$service instanceof Nette\Application\IRouter) {
			throw new Nette\UnexpectedValueException('Unable to create service \'routing.router\', value returned by factory is not Nette\Application\IRouter type.');
		}
		return $service;
	}


	/**
	 * @return Nette\Security\User
	 */
	public function createServiceSecurity__user()
	{
		$service = new Nette\Security\User($this->getService('security.userStorage'));
		$this->getService('tracy.bar')->addPanel(new Nette\Bridges\SecurityTracy\UserPanel($service));
		return $service;
	}


	/**
	 * @return Nette\Security\IUserStorage
	 */
	public function createServiceSecurity__userStorage()
	{
		$service = new Nette\Http\UserStorage($this->getService('session.session'));
		return $service;
	}


	/**
	 * @return Nette\Http\Session
	 */
	public function createServiceSession__session()
	{
		$service = new Nette\Http\Session($this->getService('http.request'), $this->getService('http.response'));
		$service->setExpiration('14 days');
		$service->setOptions(array(
			'savePath' => '/var/www/tccl/app/../temp/sessions',
		));
		return $service;
	}


	/**
	 * @return Tracy\Bar
	 */
	public function createServiceTracy__bar()
	{
		$service = Tracy\Debugger::getBar();
		if (!$service instanceof Tracy\Bar) {
			throw new Nette\UnexpectedValueException('Unable to create service \'tracy.bar\', value returned by factory is not Tracy\Bar type.');
		}
		return $service;
	}


	/**
	 * @return Tracy\BlueScreen
	 */
	public function createServiceTracy__blueScreen()
	{
		$service = Tracy\Debugger::getBlueScreen();
		if (!$service instanceof Tracy\BlueScreen) {
			throw new Nette\UnexpectedValueException('Unable to create service \'tracy.blueScreen\', value returned by factory is not Tracy\BlueScreen type.');
		}
		return $service;
	}


	/**
	 * @return Tracy\ILogger
	 */
	public function createServiceTracy__logger()
	{
		$service = Tracy\Debugger::getLogger();
		if (!$service instanceof Tracy\ILogger) {
			throw new Nette\UnexpectedValueException('Unable to create service \'tracy.logger\', value returned by factory is not Tracy\ILogger type.');
		}
		return $service;
	}


	/**
	 * @return WebLoader\Compiler
	 */
	public function createServiceWebloader__cssDefaultCompiler()
	{
		$service = new WebLoader\Compiler($this->getService('webloader.cssDefaultFiles'), $this->getService('webloader.cssNamingConvention'),
			'/var/www/tccl/www/webtemp');
		$service->setJoinFiles(TRUE);
		$service->addFileFilter($this->getService('wlCssFilter'));
		return $service;
	}


	/**
	 * @return WebLoader\FileCollection
	 */
	public function createServiceWebloader__cssDefaultFiles()
	{
		$service = new WebLoader\FileCollection('/var/www/tccl/www/css');
		$service->addFile('/var/www/tccl/www/bower/bootstrap/dist/css/bootstrap.css');
		$service->addFile('/var/www/tccl/www/bower/fontawesome/css/font-awesome.css');
		$service->addFile('/var/www/tccl/www/css/bootstrap.css');
		$service->addFile('/var/www/tccl/www/css/main.css');
		$service->addRemoteFiles(array());
		return $service;
	}


	/**
	 * @return WebLoader\DefaultOutputNamingConvention
	 */
	public function createServiceWebloader__cssNamingConvention()
	{
		$service = WebLoader\DefaultOutputNamingConvention::createCssConvention();
		if (!$service instanceof WebLoader\DefaultOutputNamingConvention) {
			throw new Nette\UnexpectedValueException('Unable to create service \'webloader.cssNamingConvention\', value returned by factory is not WebLoader\DefaultOutputNamingConvention type.');
		}
		return $service;
	}


	/**
	 * @return WebLoader\Compiler
	 */
	public function createServiceWebloader__cssPrintCompiler()
	{
		$service = new WebLoader\Compiler($this->getService('webloader.cssPrintFiles'), $this->getService('webloader.cssNamingConvention'),
			'/var/www/tccl/www/webtemp');
		$service->setJoinFiles(TRUE);
		return $service;
	}


	/**
	 * @return WebLoader\FileCollection
	 */
	public function createServiceWebloader__cssPrintFiles()
	{
		$service = new WebLoader\FileCollection('/var/www/tccl/www/css');
		$service->addRemoteFiles(array());
		return $service;
	}


	/**
	 * @return WebLoader\LoaderFactory
	 */
	public function createServiceWebloader__factory()
	{
		$service = new WebLoader\LoaderFactory(array(
			'default' => 'webtemp',
			'print' => 'webtemp',
		), $this->getService('http.request'), $this);
		return $service;
	}


	/**
	 * @return WebLoader\Compiler
	 */
	public function createServiceWebloader__jsDefaultCompiler()
	{
		$service = new WebLoader\Compiler($this->getService('webloader.jsDefaultFiles'), $this->getService('webloader.jsNamingConvention'),
			'/var/www/tccl/www/webtemp');
		$service->setJoinFiles(TRUE);
		return $service;
	}


	/**
	 * @return WebLoader\FileCollection
	 */
	public function createServiceWebloader__jsDefaultFiles()
	{
		$service = new WebLoader\FileCollection('/var/www/tccl/www/js');
		$service->addFile('/var/www/tccl/www/bower/jquery/jquery.min.js');
		$service->addFile('/var/www/tccl/www/bower/bootstrap/dist/js/bootstrap.min.js');
		$service->addFile('/var/www/tccl/www/js/lib/bootstrap-check-buttons.js');
		$service->addFile('/var/www/tccl/www/bower/nette.ajax.js/nette.ajax.js');
		$service->addFile('/var/www/tccl/www/js/nextras.datagrid.js');
		$service->addFile('/var/www/tccl/www/js/netteForms.js');
		$service->addFile('/var/www/tccl/www/js/confirm.dialog.js');
		$service->addFile('/var/www/tccl/www/js/main.js');
		$service->addRemoteFiles(array());
		return $service;
	}


	/**
	 * @return WebLoader\DefaultOutputNamingConvention
	 */
	public function createServiceWebloader__jsNamingConvention()
	{
		$service = WebLoader\DefaultOutputNamingConvention::createJsConvention();
		if (!$service instanceof WebLoader\DefaultOutputNamingConvention) {
			throw new Nette\UnexpectedValueException('Unable to create service \'webloader.jsNamingConvention\', value returned by factory is not WebLoader\DefaultOutputNamingConvention type.');
		}
		return $service;
	}


	/**
	 * @return WebLoader\Filter\CssUrlsFilter
	 */
	public function createServiceWlCssFilter()
	{
		$service = new WebLoader\Filter\CssUrlsFilter('/var/www/tccl/www', '/');
		return $service;
	}


	public function initialize()
	{
		Kdyby\Doctrine\Proxy\ProxyAutoloader::create('/var/www/tccl/app/../temp/proxies', 'Kdyby\GeneratedProxy')->register();Doctrine\Common\Annotations\AnnotationRegistry::registerLoader("class_exists");
		date_default_timezone_set('Europe/Prague');
		header('X-Frame-Options: SAMEORIGIN');
		header('X-Powered-By: Nette Framework');
		header('Content-Type: text/html; charset=utf-8');
		Nette\Reflection\AnnotationsParser::setCacheStorage($this->getByType("Nette\Caching\IStorage"));
		Nette\Reflection\AnnotationsParser::$autoRefresh = TRUE;
		$this->getService('session.session')->exists() && $this->getService('session.session')->start();
		Tracy\Debugger::$maxLen = 500;
		Tracy\Debugger::$maxDepth = 10;
		Tracy\Debugger::$showLocation = TRUE;

		Kdyby\Doctrine\Diagnostics\Panel::registerBluescreen($this);
		Tracy\Debugger::getBlueScreen()->collapsePaths[] = '/var/www/tccl/vendor/kdyby/doctrine/src/Kdyby/Doctrine';
		Tracy\Debugger::getBlueScreen()->collapsePaths[] = '/var/www/tccl/vendor/doctrine';
		Tracy\Debugger::getBlueScreen()->collapsePaths[] = '/var/www/tccl/app/../temp/proxies';
	}

}



final class Container_fdc70f1db7_Kdyby_Doctrine_DI_IRepositoryFactoryImpl_doctrine_repositoryFactory_default_defaultRepositoryFactory implements Kdyby\Doctrine\DI\IRepositoryFactory
{
	private $container;


	public function __construct(Container_fdc70f1db7 $container)
	{
		$this->container = $container;
	}


	public function create(Doctrine\ORM\EntityManagerInterface $entityManager, Doctrine\ORM\Mapping\ClassMetadata $classMetadata)
	{
		$service = new Kdyby\Doctrine\EntityRepository($entityManager, $classMetadata);
		return $service;
	}

}



final class Container_fdc70f1db7_Nette_Bridges_ApplicationLatte_ILatteFactoryImpl_latte_latteFactory implements Nette\Bridges\ApplicationLatte\ILatteFactory
{
	private $container;


	public function __construct(Container_fdc70f1db7 $container)
	{
		$this->container = $container;
	}


	public function create()
	{
		$service = new Latte\Engine;
		$service->setTempDirectory('/var/www/tccl/app/../temp/cache/latte');
		$service->setAutoRefresh(TRUE);
		$service->setContentType('html');
		Nette\Utils\Html::$xhtml = FALSE;
		$service->onCompile[] = function ($engine) { Nextras\Latte\Macros\RedefineMacro::install($engine->getCompiler()); };
		$service->onCompile[] = function ($engine) { Instante\Bootstrap3Renderer\Latte\FormMacros::install($engine->getCompiler()); };
		return $service;
	}

}
