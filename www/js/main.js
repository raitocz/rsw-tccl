

$(function () {
    $.nette.init();


    $('[data-toggle="tooltip"]').tooltip();
    $('.markdown-editor').markdown({
        height: 500,
        resize: 'vertical',
        iconlibrary: 'fa',
        onPreview: function (e) {
            return markdown.toHTML(e.getContent());
        },
    });
});