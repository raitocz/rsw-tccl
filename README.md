The Crew Car list
=============

Car list for the game The Crew

Requirements
----------
PotstgreSQL >= 9.3 
PHP 5.6
Composer & Bower
Rewrite module in Apache

Installing
----------

1. Use **Composer** and **Bower** to install required libraries
2. Create **temp/**, **log/** and **www/webtemp/** directories in application root with write permissions for your webserver
3. Create database named "tccl"
4. Set your DB details in **app/config/config.neon**
5. Run via terminal:

```
#!bash

php www/index.php migrations:continue
```




License
-------
GNU General Public License, version 3 (GPL-3.0)
https://opensource.org/licenses/GPL-3.0