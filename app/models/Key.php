<?php

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;

trait Key {

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $key;

	/**
	 * @return string
	 */
	final public function getKey() {
		return $this->key;
	}

	/**
	 * @return string
	 */
	final public function setKey($key) {
		return $this->key = $key;
	}

}
