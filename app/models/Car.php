<?php

namespace App\Models\Car;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Car {

	use \Kdyby\Doctrine\Entities\Attributes\Identifier;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Models\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * @var int
	 */
	protected $owner;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	protected $customname;

	/**
	 * @ORM\ManyToOne(targetEntity="CarManufacturer")
	 * @ORM\JoinColumn(name="carmanufacturer_id", referencedColumnName="id")
	 * @var CarManufacturer
	 */
	protected $manufacturer;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $model;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $label;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $engineCc;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $engineVentils;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $enginePower;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $engineTorque;

	/**
	 * @ORM\ManyToOne(targetEntity="CarBodyType")
	 * @ORM\JoinColumn(name="carbodytype_id", referencedColumnName="id")
	 * @var int
	 */
	protected $bodytype;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $mileage;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 */
	protected $mileageDate;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	protected $yearMade;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var \DateTime
	 */
	protected $dateBought;

	/**
	 * @ORM\ManyToOne(targetEntity="CarPhoto")
	 * @ORM\JoinColumn(name="carphoto_id", referencedColumnName="id")
	 * @var int
	 */
	protected $mainPhoto;

	/**
	 * @ORM\ManyToOne(targetEntity="CarPhoto")
	 * @ORM\JoinColumn(name="carphoto_id", referencedColumnName="id")
	 * @var int
	 */
	protected $bgPhoto;

	// -- METHODS
	// -----------------------------------------------------------------------------------------------------------------

	public function getShowname() {
		return $this->getManufacturer()->getName() . ' ' . $this->getModel();
	}

	// -- SETTERS & GETTERS
	// -----------------------------------------------------------------------------------------------------------------

	public function getOwner() {
		return $this->owner;
	}

	public function getCustomname() {
		return $this->customname;
	}

	public function getManufacturer() {
		return $this->manufacturer;
	}

	public function getModel() {
		return $this->model;
	}

	public function getLabel() {
		return $this->label;
	}

	public function getEngineCc() {
		return $this->engineCc;
	}

	public function getEngineVentils() {
		return $this->engineVentils;
	}

	public function getEnginePower() {
		return $this->enginePower;
	}

	public function getEngineTorque() {
		return $this->engineTorque;
	}

	public function getBodytype() {
		return $this->bodytype;
	}

	public function getMileage() {
		return $this->mileage;
	}

	public function getMileageDate() {
		return $this->mileageDate;
	}

	public function getYearMade() {
		return $this->yearMade;
	}

	public function getDateBought() {
		return $this->dateBought;
	}

	public function getMainPhoto() {
		return $this->mainPhoto;
	}

	public function getBgPhoto() {
		return $this->bgPhoto;
	}

	public function setOwner($owner) {
		$this->owner = $owner;
		return $this;
	}

	public function setCustomname($customname) {
		$this->customname = $customname;
		return $this;
	}

	public function setManufacturer($manufacturer) {
		$this->manufacturer = $manufacturer;
		return $this;
	}

	public function setModel($model) {
		$this->model = $model;
		return $this;
	}

	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	public function setEngineCc($engineCc) {
		$this->engineCc = $engineCc;
		return $this;
	}

	public function setEngineVentils($engineVentils) {
		$this->engineVentils = $engineVentils;
		return $this;
	}

	public function setEnginePower($enginePower) {
		$this->enginePower = $enginePower;
		return $this;
	}

	public function setEngineTorque($engineTorque) {
		$this->engineTorque = $engineTorque;
		return $this;
	}

	public function setBodytype($bodytype) {
		$this->bodytype = $bodytype;
		return $this;
	}

	public function setMileage($mileage) {
		$this->mileage = $mileage;
		return $this;
	}

	public function setMileageDate(\DateTime $mileageDate) {
		$this->mileageDate = $mileageDate;
		return $this;
	}

	public function setYearMade(\DateTime $yearMade) {
		$this->yearMade = $yearMade;
		return $this;
	}

	public function setDateBought(\DateTime $dateBought) {
		$this->dateBought = $dateBought;
		return $this;
	}

	public function setMainPhoto($mainPhoto) {
		$this->mainPhoto = $mainPhoto;
		return $this;
	}

	public function setBgPhoto($bgPhoto) {
		$this->bgPhoto = $bgPhoto;
		return $this;
	}

}
