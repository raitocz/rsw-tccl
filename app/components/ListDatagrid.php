<?php

namespace App\Components;

use Nette;

class ListDatagrid extends Nette\Object {

	public function __construct() {
	}

	// TODO: Translate strings
	public function create() {
		$grid = new \Nextras\Datagrid\Datagrid;

		$grid->addColumn('id')->enableSort(\Nextras\Datagrid\Datagrid::ORDER_ASC);

		$grid->addColumn('email', 'Email')->enableSort();

		$grid->addColumn('nickname', 'Přezdívka')->enableSort();

		$grid->addColumn('name', 'Jméno')->enableSort();

		$grid->addColumn('surname', 'Příjmení')->enableSort();

		$grid->addColumn('role_id', 'Role')->enableSort();

		$grid->addColumn('country_id', 'Stát')->enableSort();

		$grid->addColumn('city', 'Město')->enableSort();

		$grid->addColumn('registered', 'Registrován')->enableSort();

		$grid->addColumn('deleted', 'Smazán')->enableSort();

		$grid->setFilterFormFactory(function () {
			$form = new Nette\Forms\Container;
			$form->addText('email');
			$form->addText('nickname');
			$form->addText('name');
			$form->addText('surname');
			$form->addSelect('deleted', NULL, array(
					'all' => $this->translator->translate('texts.admin.users.listAll'),
					'deleted' => $this->translator->translate('texts.admin.users.listDeleted'),
					'nondeleted' => $this->translator->translate('texts.admin.users.listNonDeleted')
				))->setDefaultValue('nondeleted');

			$form->addSelect('role_id', NULL, $this->database->table('role')->fetchPairs('id', 'name'))->setPrompt('---');

			$form->addSelect('country_id', NULL, $this->database->table('country')->fetchPairs('id', 'name'))->setPrompt('---');

			return $form;
		});

		$grid->setPagination(20, $this->getDataSourceSum);

		$grid->setRowPrimaryKey('id');

		$grid->setDataSourceCallback($this->getDataSource);

		$grid->addCellsTemplate($this->applicationController->getAppDir() . '/templates/@bootstrap3.datagrid.latte');
		$grid->addCellsTemplate(__DIR__ . '/@cells.latte');

		return $grid;
	}

	public function getDataSource($filter, $order, Nette\Utils\Paginator $paginator = NULL) {
		$selection = $this->prepareDataSource($filter, $order);
		if ($paginator) {
			$selection->limit($paginator->getItemsPerPage(), $paginator->getOffset());
		}

		return $selection;
	}

	public function getDataSourceSum($filter, $order) {
		return $this->prepareDataSource($filter, $order)->count('*');
	}

	private function prepareDataSource($filter, $order) {
		$filters = array();
		foreach ($filter as $k => $v) {
			if (($k === 'id' || $k === 'role_id' || $k === 'country_id') || is_array($v)) {
				$filters[$k] = $v;
			}
			elseif ($k == 'deleted') {
				if ($v == 'deleted') {
					$filters[] = $k . ' IS NOT NULL';
				}
				elseif ($v == 'nondeleted') {
					$filters[] = $k . ' IS NULL';
				}
			}
			else {
				$filters[$k . ' LIKE ?'] = "%$v%";
			}
		}
		$selection = $this->database->table('user')->where($filters);
		if ($order) {
			$selection->order(implode(' ', $order));
		}

		return $selection;
	}

}